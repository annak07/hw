// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.


class Phone {
   constructor(options) {
      this.number = options.number
      this.model = options.model
      this.weight = options.weight
   }
   receiveCall() {
      console.log("Телефонує Taras")
   }
   getNumber() {
      console.log(`${this.number}`)
   }
}

const phone = new Phone({
   number: '0954565145',
   model: 'apple',
   weight: '350',
})
console.log(phone);
