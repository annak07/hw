
'use strict';

// Завдання №1
let doc = {
   header: "header",
   body: "body",
   footer: "footer",
   date: "date",
   addition: {
      header: "header",
      body: "body",
      footer: "footer",
      date: "date",
   },
   showDoc() {
      document.write(`${doc.header},<br/> ${doc.body},<br/> ${doc.footer},<br/> ${doc.date}, <br/>
      ${doc.addition.header}, <br/> ${doc.addition.body}, <br/> ${doc.addition.footer},  <br/>${doc.addition.date}`)
   }
}
doc.showDoc();

for (let key in doc) {
   console.log(key);
   console.log(doc[key]);
}

for (let key in doc.addition) {
   console.log(key);
   console.log(doc.addition[key]);
}
// Завдання №2
function map(fn, array) {
   let result = []
   for (let i = 0; i < array.length; i++) {
      result.push(fn(array[i]));
   }
   return result;
};
const mult = (value) => value;
console.log(map(mult, [10, 20, 30]));

// // Завдання №3

let fn = function (i) {
   document.write(`<div class='wrapper'>    
   <div id='number'>Number: ${i.id}</div>      
   <div id='name'>Name: ${i.name}</div>        
   <div id="mail">Email: ${i.email}</div>      
   <div id="review">Commit: "${i.body}"</div>   
   </div>`);
}


arr.forEach(fn);

